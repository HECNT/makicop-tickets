// var url = helpers.getUrl();

angular.module(MODULE_NAME)
.service('HomeService', ['$http', function($http) {
  var url = "https://localhost:3003";
  var urlBase = url + '/home';

  this.doLogin = function(d) {
    return $http.post(urlBase + '/login', d);
  }

  this.doLogOut = function() {
    return $http.get(urlBase + '/kill');
  }

  this.getInfoUser = function(d) {
    return $http.post(urlBase + '/get-info-user', d);
  }

  this.verificaPass = function(d) {
    return $http.post(urlBase + '/verifica-pass', d);
  }

  this.cambiaPass = function(d) {
    return $http.post(urlBase + '/cambia-pass', d);
  }

  this.setUpdate = function(d) {
    return $http.post(urlBase + '/set-update', d);
  }

  this.getTicket = function() {
    return $http.get(urlBase + '/get-ticket');
  }

  this.getDepartamento = function() {
    return $http.get(urlBase + '/get-departamento');
  }

  this.getEstatus = function() {
    return $http.get(urlBase + '/get-estatus');
  }

  this.getServicio = function() {
    return $http.get(urlBase + '/get-servicio');
  }

  this.checkAvalible = function(d) {
    return $http.post(urlBase + '/check-avalible', d);
  }

  this.setTicket = function(d) {
    return $http.post(urlBase + '/set-ticket', d);
  }

  this.cancelTicket = function(d) {
    return $http.post(urlBase + '/cancelar-ticket', d);
  }

  this.updateTicket = function(d) {
    return $http.post(urlBase + '/update-ticket', d);
  }

  this.getPerfil = function(d) {
    return $http.get(urlBase + '/get-perfil');
  }

  this.createUser = function(d) {
    return $http.post(urlBase + '/create-user', d);
  }

  this.createUserSistema = function(d) {
    return $http.post(urlBase + '/create-user-sistema', d);
  }

}]);
