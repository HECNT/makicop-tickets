require('../services/home');
var swal = require('sweetalert');

angular.module(MODULE_NAME)
.controller('ticketCtrl', ['$scope', 'HomeService', '$timeout', 'socket', function($scope, HomeService, $timeout, socket) {
  
  $scope.init = init;
  $scope.btnModalDetalle = btnModalDetalle
  $scope.initNuevo = initNuevo;
  $scope.btnCreaTicket = btnCreaTicket;
  $scope.btnCancelarTicket = btnCancelarTicket;
  $scope.btnUpdateTicket = btnUpdateTicket;

  $scope.list = [];
  $scope.ticket = {empresa: "", departamento: "", servicio: "", correo: "", descripcion: ""};
  $scope.act = {estatus: "", comentario: ""};
  $scope.buscar = "";

  $scope.load = true;
  $scope.msg_warn = false

  function init() {
      HomeService.getTicket()
      .success(function(res){
          if (res.err) {
              $scope.load = false;
              alert('Hubo un error')
          } else {
              $scope.list = res.data
              HomeService.getEstatus()
              .success(function(res){
                  if (res.err) {
                    $scope.load = false;
                    swal({
                        icon: "error",
                        title: "Hubo un error",
                        text: "Al tratar de traer el catalogo de estatus"
                    })  
                  } else {
                    $scope.load = false;
                    $scope.listEstatus = res.data;
                  }
              })
          }
      })
  }

  function btnModalDetalle(d) {
    $scope.detalle = d
    $scope.act.estatus = "";
    $('#modal-detalle').modal('show');
  }

  function initNuevo(usuario_id) {
      HomeService.getDepartamento()
      .success(function(res){
          if (res.err) {
              alert('Hubo un error al tratar de traer el catalogo de departamento')
          } else {
              $scope.listDepartamento = res.data
              HomeService.getEstatus()
              .success(function(res){
                  if (res.err) {
                    alert('Hubo un error al tratar de traer el catalogo de estatus')
                  } else {
                    $scope.listEstatus = res.data
                    HomeService.getServicio()
                    .success(function(res){
                        if (res.err) {
                            alert('Hubo un error al tratar de traer el catalogo de servicio')
                        } else {
                            $scope.listServicio = res.data
                            HomeService.getInfoUser({usuario_id: usuario_id})
                            .success(function(res){
                                $scope.usuario = res.data[0];
                                $scope.ticket.empresa = res.data[0].empresa;
                                $scope.ticket.correo = res.data[0].correo;
                                HomeService.checkAvalible({usuario_id: usuario_id})
                                .success(function(res){
                                    $scope.load = false;
                                    if (res.data.length > 0) {
                                        $scope.msg_warn = true
                                    } else {
                                        $scope.msg_warn = false
                                    }
                                })
                            })
                        }
                    })
                  }
              })
          }
      })
  }

  function btnCreaTicket() {
      var d = $scope.ticket;
      if ( d.correo.length === 0 || d.empresa.length === 0 || d.departamento.length === 0 || d.servicio.length === 0 || d.descripcion.length === 0 ) {
        swal({
            icon: "error",
            title: "Tienes un error",
            text: "Todos los campos son requeridos"
        })
      } else {
        if (!isEmailAddress(d.correo)) {
            swal({
                icon: "error",
                title: "Tienes un error",
                text: "El correo ingreado no es valido"
            })  
        } else {
            HomeService.setTicket(d)
            .success(function(res){
                if (res.err) {
                    swal({
                        icon: "error",
                        title: "Hubo un error",
                        text: "Al tratar de generar el ticket"
                    })      
                } else {
                    swal({
                        icon: "success",
                        title: "Todo bien",
                        text: "Se creo el ticket"
                    }).then(function(){
                        location.reload();
                    })      
                }
            })
        }
      }
  }

  function btnCancelarTicket() {
      var d = $scope.detalle;
      HomeService.cancelTicket({ticket_id: d.ticket_id})
      .success(function(res){
          if (res.err) {
            swal({
                icon: "error",
                title: "Hubo un error",
                text: "Al tratar de cancelar el ticket"
            }) 
          } else {
            swal({
                icon: "success",
                title: "Todo bien",
                text: "Se cancelo el ticket"
            }).then(function(){
                location.reload();
            }) 
          }
      })
  }

  function btnUpdateTicket() {
      var d = $scope.act
      d.ticket_id = $scope.detalle.ticket_id;
      if (d.estatus.length === 0) {
        swal({
            icon: "error",
            title: "Tienes un error",
            text: "Tienes que seleccionar un estatus!"
        }) 
      } else {
          if (d.estatus === '5') {
              if ( d.comentario.length === 0 ) {
                swal({
                    icon: "error",
                    title: "Tienes un error",
                    text: "Debes de agregar un comentario"
                })    
                return 0;   
              }
          }
            HomeService.updateTicket(d)
            .success(function(res){
                if (res.err) {
                    swal({
                        icon: "error",
                        title: "Hubo un error",
                        text: "Al tratar de actualizar el ticket"
                    })  
                } else {
                    swal({
                        icon: "success",
                        title: "Todo bien.",
                        text: "Se actualizo el ticket"
                    }).then(function(){
                        location.reload();
                    })
                }
            })
      }
  }

  var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  function isEmailAddress(str) {
      if (str.match(pattern) === null) {
        return false;    
      } else {
        return true;
      }
  }

}]);