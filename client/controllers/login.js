require('../services/home');
var axios = require("axios");

angular.module(MODULE_NAME)
.controller('loginCtrl', ['$scope', 'HomeService', '$timeout', 'socket', function($scope, HomeService, $timeout, socket) {
  
    $scope.login = {};
    $scope.btnLogin = btnLogin;
    $scope.msg_show = false;
    $scope.loadingLog = true;

    function btnLogin() {
        $scope.loadingLog = false;
        var d = $scope.login;
        HomeService.doLogin(d)
        .success(function(res){
            
            if (res.err) {
                $scope.loadingLog = true;
                $scope.msg_show = true;
                $scope.err_msg = res.description;
            } else {
                $scope.loadingLog = true;
                $scope.msg_show = false;
                location.reload();
            }
        })
    }    
  

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
