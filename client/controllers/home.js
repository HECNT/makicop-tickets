require('../services/home');


angular.module(MODULE_NAME)
.controller('homeCtrl', ['$scope', 'HomeService', '$timeout', 'socket', function($scope, HomeService, $timeout, socket) {
  
  $scope.btnCierra = btnCierra;
  $scope.btnMenu = btnMenu;

  function btnMenu() {
    $('.ui.sidebar').sidebar('toggle');
  }

  function btnCierra() {
    HomeService.doLogOut()
    .success(function(res){
      location.reload()
    })
  }

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
