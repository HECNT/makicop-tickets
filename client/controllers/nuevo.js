require('../services/home');
var swal = require('sweetalert');

angular.module(MODULE_NAME)
.controller('nuevoCtrl', ['$scope', 'HomeService', '$timeout', 'socket', function($scope, HomeService, $timeout, socket) {
  
  $scope.init = init;
  $scope.createUser = createUser;
  $scope.btnCreateUserSistema = btnCreateUserSistema;

  $scope.validateUser = true;

  $scope.nuevo = { nombre: "", ap: "", am: "", edad: "", correo: "", usuario_login: "", empresa: "", perfil: "" }

  function init() {
      HomeService.getPerfil()
      .success(function(res){
          if (res.err) {
            swal({
                icon: "error",
                title: "Hubo un error",
                text: "Hubo un error al tratar de traer los perfiles"
            })  
          } else {
            $scope.listPerfil = res.data; 
          }
      })
  }

  function createUser() {
      var d = { user_login: $scope.nuevo.usuario_login };
      HomeService.createUser(d)
      .success(function(res){
          if (res.err) {
            swal({
                icon: "error",
                title: "Hubo un error",
                text: "Hubo un error al tratar de crear el usuario"
            })  
          } else {
              $scope.validateUser = false;
          }
      })
  }

  function btnCreateUserSistema() {
      var d = $scope.nuevo;
      if (d.nombre === undefined || d.ap === undefined || d.am === undefined || d.edad === undefined || d.correo === undefined || d.usuario_login === undefined || d.empresa === undefined || d.perfil === undefined ) {
        swal({
            icon: "error",
            title: "Hubo un error",
            text: "Todos los campos son necesarios para poder continuar"
        })  
      } else {
        HomeService.createUserSistema(d)
        .success(function(res){
            if (res.err) {
                alert('Hubo un error al crear el usuario')
            } else {
                swal({
                    icon: "success",
                    title: "Todo bien",
                    text: "Se creo el usuario"
                }).then(function(){
                    location.reload();
                })    
            }
        })
      }
  }

}]);

    angular.module(MODULE_NAME)
    .directive('fileModel', ['$parse', function ($parse) {
        return {
           restrict: 'A',
           link: function(scope, element, attrs) {
              var model = $parse(attrs.fileModel);
              var modelSetter = model.assign;
              element.bind('change', function(){
                 scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                 });
              });
           }
        };
     }]);
