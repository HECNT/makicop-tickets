require('../services/home');
var swal = require('sweetalert');

angular.module(MODULE_NAME)
.controller('perfilCtrl', ['$scope', 'HomeService', '$timeout', 'socket', function($scope, HomeService, $timeout, socket) {
  
  $scope.init = init;
  $scope.btnModalCambiar = btnModalCambiar;
  $scope.btnCambia = btnCambia;
  $scope.verificaPass = verificaPass;
  $scope.btnEditar = btnEditar;
  $scope.btnCancelar = btnCancelar;
  $scope.btnEditarGuardar = btnEditarGuardar;

  $scope.perfil = {}
  $scope.cambia = {actual: "", nueva:"", repite:""}

  $scope.load = true;
  $scope.msg_cambia_err = false;
  $scope.disabled_btn = true;
  $scope.form_user = true;
  $scope.toggleEdit = true;
  $scope.msg_form_edit = false;
  $scope.btn_loading_edit = false;

  $scope.msg_cambia_text = "";

  function init(usuario_id) {
    $scope.load = true;
      var item = {usuario_id: usuario_id}
      HomeService.getInfoUser(item)
      .success(function(res){
        if (res.err) {
            $scope.load = false;
            alert('Hubo un error al traer la información')
        } else {
            $scope.load = false;
            $scope.perfil = res.data[0];
        }
      })
  }

  function btnModalCambiar() {
    $('#modal-cambiar').modal('show');
  }

  function btnCambia() {
      var d = $scope.cambia;
      if (d.actual.length === 0 || d.nueva.length === 0 || d.repite.length === 0) {
          $scope.msg_cambia_err = true;
          $scope.msg_cambia_text = "Todos los campos son necesarios para continuar.";
      } else {
        $scope.msg_cambia_err = false;
        HomeService.cambiaPass(d)
        .success(function(res){
            if (res.err) {
                $scope.msg_cambia_err = true;
                $scope.msg_cambia_text = res.description;
            } else {
                $scope.msg_cambia_err = false;
                $scope.msg_cambia_text = "";
                swal({
                    icon: "success",
                    title: "Contraseña cambiada",
                    text: "Vuelve a iniciar sesión"
                }).then(function(){
                    HomeService.doLogOut()
                    .success(function(res){
                        location.reload()
                    })
                })
            }
        })
      }
  }

  function verificaPass() {
      var d = $scope.cambia;
      HomeService.verificaPass(d)
      .success(function(res){
          if (res.err) {
            $scope.msg_cambia_err = true;
            $scope.msg_cambia_text = "La contraseña introducida no es valida";
          } else {
            if (res.data[0].res === 2) {
                $scope.disabled_btn = false;
                $scope.msg_cambia_err = false;
                $scope.msg_cambia_text = "";
            } else {
                alert('Hay un error al verificar la contraseña')
            }
          }
      })
  }

  function btnEditar() {
      $scope.form_user = false;
      $scope.toggleEdit = false;
  }
  
  function btnCancelar() {
      location.reload();
  }

  function btnEditarGuardar() {
    $scope.btn_loading_edit = true;
      var d = $scope.perfil;
      if ( d.nombre.length === 0 || d.ap.length === 0 || d.am.length === 0 || d.edad.length === 0 || d.correo.length === 0 ) {
        $scope.msg_form_edit = true;
      } else {
        $scope.msg_form_edit = false;
        if (isNaN(d.edad)) {
            swal({
                icon: "error",
                title: "Tienes un error",
                text: "El campo edad tiene que ser numerico"
            })
            $scope.btn_loading_edit = false;
        } else {
            if (d.edad < 5 || d.edad > 80) {
                swal({
                    icon: "error",
                    title: "Tienes un error",
                    text: "El campo edad no puede ser menor a 5 o mayor a 80"
                })  
                $scope.btn_loading_edit = false;
            } else {
                if (!isEmailAddress(d.correo)) {
                    swal({
                        icon: "error",
                        title: "Tienes un error",
                        text: "El campo correo no es valido"
                    })  
                    $scope.btn_loading_edit = false;
                } else {
                    HomeService.setUpdate(d)
                    .success(function(res){
                        if (res.err) {
                            swal({
                                icon: "error",
                                title: "Tienes un error",
                                text: res.description
                            }) 
                            $scope.btn_loading_edit = false;
                        } else {
                            $scope.btn_loading_edit = false;
                            swal({
                                icon: "success",
                                title: "Datos actualizados",
                                text: "Se actualizaron los datos"
                            }).then(function(){
                                location.reload()
                            })
                        }
                    })
                }
            }
        }
      }
  }

  var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  function isEmailAddress(str) {
      if (str.match(pattern) === null) {
        return false;    
      } else {
        return true;
      }
  }

}]);