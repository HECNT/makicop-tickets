var fs = require('fs');
var mysql = require('mysql');

module.exports.getUser = function(d) {
  return new Promise(function(resolve, reject){
      var conn = fs.readFileSync(__dirname + '/conn') ;
      conn = JSON.parse(conn);
      
      conn.user = d.user
      conn.password = d.holi
      conn.database = d.database
      
      var connection = mysql.createConnection(conn);

      connection.connect();

      var query = `
        SELECT 
          usuario_id,
          usuario_login,
          empresa, 
          perfil_id,
          correo
        FROM 
          usuario 
        WHERE 
          usuario_login = ?
      `

      connection.query(query, [d.user], function(err, result){
          if (err) {
              resolve({err: true, description: err});
              connection.end();
          } else {
              resolve({err: false, data: result});
              connection.end();
          }
      })
  })
}

module.exports.getInfoUser = function(d) {
  return new Promise(function(resolve, reject){
      var conn = fs.readFileSync(__dirname + '/conn') ;
      conn = JSON.parse(conn);
      
      conn.user = d.user
      conn.password = d.holi
      conn.database = d.database
      
      var connection = mysql.createConnection(conn);

      connection.connect();

      var query = `
      select 
            a1.usuario_id,
            a1.nombre,
            a1.ap,
            a1.am,
            a1.edad,
            a1.correo,
            a1.empresa,
            a2.nombre as perfil
        from 
            usuario as a1
        inner join 
            perfil as a2
        on 
            a1.perfil_id = a2.perfil_id
        where 
            a1.usuario_id = ?
        and 
            a1.activo = true
      `

      connection.query(query, [d.usuario_id], function(err, result){
          if (err) {
              resolve({err: true, description: err});
              connection.end();
          } else {
              resolve({err: false, data: result});
              connection.end();
          }
      })
  })
}

module.exports.verificaPass = function(d) {
  return new Promise(function(resolve, reject){
      var conn = fs.readFileSync(__dirname + '/conn') ;
      conn = JSON.parse(conn);
      
      conn.user = d.user
      conn.password = d.holi
      conn.database = d.database
      
      var connection = mysql.createConnection(conn);

      connection.connect();

      var query = `
      SELECT 1+1 AS res
      `
      
      connection.query(query, function(err, result){
          if (err) {
              resolve({err: true, description: err});
              connection.end();
          } else {
              resolve({err: false, data: result});
              connection.end();
          }
      })
  })
}

module.exports.cambiaPassLocal = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        ALTER USER '${d.user}'@'localhost' IDENTIFIED BY '${d.nueva}';
        `
        
        connection.query(query, function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }


  module.exports.cambiaPassExterno = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        ALTER USER '${d.user}'@'%' IDENTIFIED BY '${d.nueva}';
        `
        
        connection.query(query, function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.setUpdate = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        UPDATE 
            usuario
        SET 
            nombre = ?,
            ap = ?,
            am = ?,
            edad = ?,
            correo = ?
        WHERE 
            usuario_id = ?
        `
        
        connection.query(query, [d.nombre, d.ap, d.am, d.edad, d.correo, d.usuario_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.getTicket = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var cond = d.perfil_id === 2 ? '' : 'where a1.usuario_id = ?'

        var query = `
        select 
            a1.ticket_id,
            a1.fecha,
            a1.descripcion,
            a2.nombre as nombre_departamento,
            a3.nombre as nombre_estatus,
            a4.nombre as nombre_servicio,
            a5.empresa,
            a6.usuario_id,
            a7.nombre,
            a6.comentario,
            a6.fecha as fecha_atencion
        from 
            ticket as a1
        inner join 
            departamento as a2
        on 
            a1.departamento_id = a2.departamento_id
        inner join 
            estatus as a3
        on 
            a1.estatus_id = a3.estatus_id
        inner join 
            servicio as a4
        on 
            a1.servicio_id = a4.servicio_id
        inner join 
        	usuario as a5
        on 
        	a1.usuario_id = a5.usuario_id
        left join 
        	ticket_atendido as a6 
        on 	
        	a1.ticket_id = a6.ticket_id
        left join 
        	usuario as a7 
        on 
        	a6.usuario_id = a7.usuario_id
        ${cond}
        `
        
        connection.query(query, [d.usuario_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.getDepartamento = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        select * from departamento
        `
        
        connection.query(query, [d.usuario_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.getEstatus = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        select * from estatus
        `
        
        connection.query(query, [d.usuario_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.getServicio = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        select * from servicio
        `
        
        connection.query(query, [d.usuario_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.checkAvalible = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        select 
            * 
        from 
            ticket 
        where 
            usuario_id = ?
        and 
            activo = 1
        and 
            estatus_id = 1
        `
        
        connection.query(query, [d.usuario_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.setTicket = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        insert into 
            ticket (fecha, descripcion, activo, departamento_id, estatus_id, usuario_id, servicio_id)
        values 
            (now(), ?, 1, ?, 1, ?, ?)
        `
        
        connection.query(query, [d.descripcion, d.departamento, d.usuario_id, d.servicio],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.cancelTicket = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        update 
            ticket
        set 
            activo = 0,
            estatus_id = 4
        where 
            ticket_id = ?
        `
        
        connection.query(query, [d.ticket_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.updateTicket = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        update 
            ticket
        set 
            activo = 0,
            estatus_id = ?
        where 
            ticket_id = ?
        `
        
        connection.query(query, [d.estatus, d.ticket_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.ticketTerminado = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        INSERT INTO 
            ticket_atendido (comentario, fecha, usuario_id, ticket_id)
        VALUES 
            (?, now(), ?, ?)
        
        `
        
        connection.query(query, [d.comentario, d.usuario_id, d.ticket_id],function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.lastRecord = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        SELECT 
        	a1.ticket_id,
        	a1.fecha,
        	a1.descripcion,
        	a2.nombre
        FROM 
        	ticket as a1
        INNER JOIN 
        	servicio as a2
        ON 
        	a1.servicio_id = a2.servicio_id
        ORDER BY 
        	ticket_id 
        DESC LIMIT 1
        `
        
        connection.query(query,function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.getPerfil = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        SELECT * FROM perfil
        `
        
        connection.query(query,function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }

  module.exports.createUserSistema = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        
        var connection = mysql.createConnection(conn);
  
        connection.connect();
  
        var query = `
        INSERT INTO 
            usuario (nombre, ap, am, edad, correo, usuario_login, empresa, activo, perfil_id)
        VALUES 
            (?,?,?,?,?,?,?,1,?)
        `
        
        connection.query(query,function(err, result){
            if (err) {
                resolve({err: true, description: err});
                connection.end();
            } else {
                resolve({err: false, data: result});
                connection.end();
            }
        })
    })
  }