var express = require('express');
var router = express.Router();
var ctrl = require('../controllers/home');
var fs = require('fs');
var verifyToken = require('./middleware');
var jwt = require('jsonwebtoken');
var config = require('./config');
var request = require("request");

router.post('/login', doLogin);
router.post('/get-info-user', getInfoUser);
router.post('/verifica-pass', verificaPass);
router.post('/cambia-pass', cambiaPass);
router.post('/set-update', setUpdate);
router.post('/check-avalible', checkAvalible);
router.post('/set-ticket', setTicket);
router.post('/cancelar-ticket', cancelTicket);
router.post('/update-ticket', updateTicket);
router.post('/create-user', createUser);
router.post('/create-user-sistema', createUserSistema);

router.get('/kill', doKill);
router.get('/get-ticket', getTicket);
router.get('/get-departamento', getDepartamento);
router.get('/get-estatus', getEstatus);
router.get('/get-servicio', getServicio);
router.get('/get-perfil', getPerfil);

//router.get('/', verifyToken, doSome)

function doLogin(req, res) {
  var uri = "https://35.229.59.251:5000/login";
  var d = req.body;
  var holi = d.pass;
  var item = {
    user: d.user,
    holi: holi,
    database: 'horus_tickets'
  }
  request.post({url:uri, strictSSL: false, form: item}, function(err, response, body){
    if (err) {
      res.json({err: true, description: err})
    } else {
      var json = JSON.parse(body);
      if (json.err) {
        res.json({err: true, description: json.description});
      } else {
        if (json.data[0].res === 2) {
          ctrl.getUser(item)
          .then(function(result){
            if (result.data.length > 0) {
              item.usuario_id = result.data[0].usuario_id
              item.perfil_id = result.data[0].perfil_id
              item.empresa = result.data[0].empresa
              req.session.usuario = item;
              res.json({err: false})
            } else {
              res.json({err: true, description: "El usuario ingresado no existe en este sistema"})
            }
          })
        } else {
          res.json(json);
        }
      }
    }
  })
}

function doKill(req, res) {
  req.session.usuario = null
  res.json({err: false});
}

function getInfoUser(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  ctrl.getInfoUser(d)
  .then(function(result){
    res.json(result)
  })  
}

function verificaPass(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user;
  d.holi = d.actual;
  d.database = req.session.usuario.database;
  ctrl.verificaPass(d)
  .then(function(result){
    res.json(result)
  })  
}

function cambiaPass(req, res) {
  // FALTA HACER
  var d = req.body;
  d.user = req.session.usuario.user;
  d.holi = d.actual;
  d.database = req.session.usuario.database;
  ctrl.cambiaPass(d)
  .then(function(result){
    res.json(result)
  })  
}

function setUpdate(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  ctrl.setUpdate(d)
  .then(function(result){
    res.json(result)
  })  
}

function getTicket(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  d.perfil_id = req.session.usuario.perfil_id
  ctrl.getTicket(d)
  .then(function(result){
    res.json(result)
  })  
}

function getDepartamento(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.getDepartamento(d)
  .then(function(result){
    res.json(result)
  })  
}

function getServicio(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.getServicio(d)
  .then(function(result){
    res.json(result)
  })  
}

function getEstatus(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.getEstatus(d)
  .then(function(result){
    res.json(result)
  })  
}

function checkAvalible(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.checkAvalible(d)
  .then(function(result){
    res.json(result)
  })  
}

function setTicket(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.setTicket(d)
  .then(function(result){
    res.json(result)
  })  
}

function cancelTicket(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.cancelTicket(d)
  .then(function(result){
    res.json(result)
  })  
}

function updateTicket(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.updateTicket(d)
  .then(function(result){
    res.json(result)
  })  
}

function getPerfil(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.getPerfil(d)
  .then(function(result){
    res.json(result)
  })  
}

function createUser(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  var uri = "https://35.229.59.251:5000/login/create-user";
  var item = {
    user: d.user,
    holi: d.holi,
    database: 'horus_tickets',
    rol: 'writer',
    user_create: d.user_login
  }
  request.post({url:uri, strictSSL: false, form: item}, function(err, resp, body){
    res.json(body);
  });
}

function createUserSistema(req, res) {
  var d = req.body;
  d.user = req.session.usuario.user
  d.holi = req.session.usuario.holi
  d.database = req.session.usuario.database
  d.usuario_id = req.session.usuario.usuario_id
  ctrl.createUserSistema(d)
  .then(function(result){
    res.json(result)
  })  
}

module.exports = router;
