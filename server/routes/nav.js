var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  if (req.session.usuario) {
    res.redirect('inicio')
  } else {
    res.redirect('login')
  }
});

router.get('/inicio', function(req, res) {
  if (req.session.usuario) {
    res.render('inicio', {usuario:true, url : `https://localhost:${PUERTO}/`, usuario_id: req.session.usuario.usuario_id, perfil_id: req.session.usuario.perfil_id, empresa: req.session.usuario.empresa})
  } else {
    res.redirect('login')
  }
});

router.get('/login', function(req, res) {
  if (req.session.usuario) {
    res.redirect('inicio')
  } else {
    res.render('login', {usuario: false, url: `https://localhost:${PUERTO}/`})
  }
});

router.get('/perfil', function(req, res) {
  if (req.session.usuario) {
    res.render('perfil', {usuario:true, url : `https://localhost:${PUERTO}/`, usuario_id: req.session.usuario.usuario_id, perfil_id: req.session.usuario.perfil_id, empresa: req.session.usuario.empresa})
  } else {
    res.redirect('login')
  }
});

router.get('/nuevo_ticket', function(req, res) {
  if (req.session.usuario) {
    res.render('ticket', {usuario:true, url : `https://localhost:${PUERTO}/`, usuario_id: req.session.usuario.usuario_id, perfil_id: req.session.usuario.perfil_id, empresa: req.session.usuario.empresa})
  } else {
    res.redirect('login')
  }
});

router.get('/nuevo_usuario', function(req, res) {
  if (req.session.usuario) {
    res.render('nuevo_usuario', {usuario:true, url : `https://localhost:${PUERTO}/`, usuario_id: req.session.usuario.usuario_id, perfil_id: req.session.usuario.perfil_id, empresa: req.session.usuario.empresa})
  } else {
    res.redirect('login')
  }
});

module.exports = router;
