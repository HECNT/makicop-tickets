var model = require('../models/home')
var fs = require('fs');
var path = require('path');
var email = require('../modules/mail');

module.exports.doSome = function (d) {
  return new Promise( async function(resolve, reject) {
    let aw = await model.doSome()
    aw = aw.err ? aw : {err:false, data: aw}
    resolve(aw)
  })
}

module.exports.getUser = function (d) {
  return new Promise( async function(resolve, reject) {
    model.getUser(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.getInfoUser = function (d) {
  return new Promise( async function(resolve, reject) {
    model.getInfoUser(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.verificaPass = function (d) {
  return new Promise( async function(resolve, reject) {
    model.verificaPass(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.cambiaPass = function (d) {
  return new Promise( async function(resolve, reject) {
    if (d.nueva === d.repite) {
      model.cambiaPassLocal(d)
      .then(function(res){
        resolve(res);

        model.cambiaPassExterno(d)
        .then(function(res){
          resolve(res);
        })

      }) 
    } else {
      resolve({err: true, description: "Las contraseñas no son iguales"})
    }
  })
}

module.exports.setUpdate = function (d) {
  return new Promise( async function(resolve, reject) {
    model.setUpdate(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.getTicket = function (d) {
  return new Promise( async function(resolve, reject) {
    model.getTicket(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.getDepartamento = function (d) {
  return new Promise( async function(resolve, reject) {
    model.getDepartamento(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.getServicio = function (d) {
  return new Promise( async function(resolve, reject) {
    model.getServicio(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.getEstatus = function (d) {
  return new Promise( async function(resolve, reject) {
    model.getEstatus(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.checkAvalible = function (d) {
  return new Promise( async function(resolve, reject) {
    model.checkAvalible(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.setTicket = function (d) {
  return new Promise( async function(resolve, reject) {
    model.setTicket(d)
    .then(function(res){
      model.lastRecord(d)
      .then(function(res){
        var location = path.join(__dirname, '..', 'assets', 'platilla_ticket.html');
        var body = fs.readFileSync(location, 'utf8');
        body = body.replace('${ticket_id}', res.data[0].ticket_id)
        body = body.replace('${asunto}', res.data[0].nombre)
        body = body.replace('${fecha}', res.data[0].fecha)
        body = body.replace('${descripcion}', res.data[0].descripcion)
        email.enviarMail(d.correo, body, 'Notificaciones Horus')
        resolve(res);
      })
    })
  })
}

module.exports.cancelTicket = function (d) {
  return new Promise( async function(resolve, reject) {
    model.cancelTicket(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.updateTicket = function (d) {
  return new Promise( async function(resolve, reject) {
    model.updateTicket(d)
    .then(function(res){
      if (d.estatus === '5') {
        model.ticketTerminado(d)
        .then(function(res){
          resolve(res);
        })
      } else {
        resolve(res);
      }
    })
  })
}

module.exports.getPerfil = function (d) {
  return new Promise( async function(resolve, reject) {
    model.getPerfil(d)
    .then(function(res){
      resolve(res);
    })
  })
}

module.exports.createUserSistema = function (d) {
  return new Promise( async function(resolve, reject) {
    model.createUserSistema(d)
    .then(function(res){
      resolve(res);
    })
  })
}
